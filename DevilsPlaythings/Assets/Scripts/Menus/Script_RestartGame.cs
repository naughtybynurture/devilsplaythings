﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_RestartGame : MonoBehaviour
{
    GameObject players;
    GameObject playerSpawns;
    GameObject levelManager;
    GameObject gameManager;

    private void Start()
    {
        players = GameObject.Find("Players");
        playerSpawns = GameObject.Find("Spawnpoints");
        levelManager = GameObject.Find("LevelManager");
        gameManager = GameObject.Find("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Joystick1Button0) &&
            Input.GetKey(KeyCode.Joystick2Button0) &&
            Input.GetKey(KeyCode.Joystick3Button0) &&
            Input.GetKey(KeyCode.Joystick4Button0))
        {
            Destroy(players);
            Destroy(playerSpawns);
            Destroy(levelManager);
            Destroy(gameManager);
            SceneManager.LoadScene("Scene_StartMenu");
        }
    }
}
