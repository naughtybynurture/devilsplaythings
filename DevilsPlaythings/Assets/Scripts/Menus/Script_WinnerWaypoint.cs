﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_WinnerWaypoint : MonoBehaviour
{
    Script_LevelManager levelManager;
	// Use this for initialization
	void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();

    }

    private void Update()
    {
        if (levelManager.winnerDecided)
        {
            if (levelManager.numberOfWinners == 1)
            {
                if (gameObject.name == "Waypoint1")
                {
                    levelManager.winners[0].transform.position = gameObject.transform.position;
                }
            }
            else if (levelManager.numberOfWinners == 2)
            {
                if (gameObject.name == "Waypoint1")
                {
                    levelManager.winners[0].transform.position = gameObject.transform.position;
                }
                if (gameObject.name == "Waypoint2")
                {
                    levelManager.winners[1].transform.position = gameObject.transform.position;
                }
            }
            else if (levelManager.numberOfWinners == 3)
            {
                if (gameObject.name == "Waypoint1")
                {
                    levelManager.winners[0].transform.position = gameObject.transform.position;
                }
                if (gameObject.name == "Waypoint2")
                {
                    levelManager.winners[1].transform.position = gameObject.transform.position;
                }
                if (gameObject.name == "Waypoint3")
                {
                    levelManager.winners[2].transform.position = gameObject.transform.position;
                }
            }
            else if (levelManager.numberOfWinners == 4)
            {
                if (gameObject.name == "Waypoint1")
                {
                    levelManager.winners[0].transform.position = gameObject.transform.position;
                }
                if (gameObject.name == "Waypoint2")
                {
                    levelManager.winners[1].transform.position = gameObject.transform.position;
                }
                if (gameObject.name == "Waypoint3")
                {
                    levelManager.winners[2].transform.position = gameObject.transform.position;
                }
                if (gameObject.name == "Waypoint4")
                {
                    levelManager.winners[3].transform.position = gameObject.transform.position;
                }
            }
        }
    }
}
