﻿using UnityEngine;

public class Script_PlayerManager : MonoBehaviour
{
    public bool alterPlayersPath = false;
    public Script_LevelManager players;
    public GameObject minigameWinner;

    private void Start()
    {
        players = GetComponent<Script_LevelManager>();
    }

    private void Update()
    {
        if (alterPlayersPath)
        {
            players.playerOrder[0].GetComponent<Script_PlayerMovement>().alterMovement = true;
            players.playerOrder[1].GetComponent<Script_PlayerMovement>().alterMovement = true;
            players.playerOrder[2].GetComponent<Script_PlayerMovement>().alterMovement = true;
            players.playerOrder[3].GetComponent<Script_PlayerMovement>().alterMovement = true;
        }
        if (!alterPlayersPath)
        {
            players.playerOrder[0].GetComponent<Script_PlayerMovement>().alterMovement = false;
            players.playerOrder[1].GetComponent<Script_PlayerMovement>().alterMovement = false;
            players.playerOrder[2].GetComponent<Script_PlayerMovement>().alterMovement = false;
            players.playerOrder[3].GetComponent<Script_PlayerMovement>().alterMovement = false;
        }
    }
}
