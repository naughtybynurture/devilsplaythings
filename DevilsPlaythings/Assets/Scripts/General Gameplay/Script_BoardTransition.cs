﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Script_BoardTransition : MonoBehaviour
{
    public GameObject levelManager;
    public Text minigameWinner;
    public Text turnsLeft;

	// Use this for initialization
	void Start ()
    {
        levelManager = GameObject.Find("LevelManager");
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (levelManager.GetComponent<Script_LevelManager>().minigameName == "Scene_MiniGameHotPotato")
        {
            minigameWinner.text = "Loser: " +
                levelManager.GetComponent<Script_PlayerManager>().minigameWinner.transform.GetChild(0).name;
            turnsLeft.text = "Turns left: " +
                levelManager.GetComponent<Script_LevelManager>().numberOfTurnsLeft;
        }
        else
        {
            minigameWinner.text = "Winner: " +
                levelManager.GetComponent<Script_PlayerManager>().minigameWinner.transform.GetChild(0).name;
            turnsLeft.text = "Turns left: " +
                levelManager.GetComponent<Script_LevelManager>().numberOfTurnsLeft;
        }

        if ((Input.GetKey(KeyCode.Joystick1Button0) &&
                         Input.GetKey(KeyCode.Joystick2Button0) &&
                         Input.GetKey(KeyCode.Joystick3Button0) &&
                         Input.GetKey(KeyCode.Joystick4Button0)) ||
                         Input.GetKey(KeyCode.L))
        {
            if (levelManager.GetComponent<Script_LevelManager>().numberOfTurnsLeft >= 1)
            {
                levelManager.GetComponent<Script_LevelManager>().currentTurn.StartTurns();

            }
            SceneManager.LoadScene(levelManager.GetComponent<Script_LevelManager>().boardName);
        }
    }
}
