﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Script_Player : MonoBehaviour
{
    public int souls;
    public int silverPieces;
    public int numberOfMoveSpaces = 0;
    public int originalController;
    public int turnOrder = 0;
    public bool myTurn;
    public bool readyForMinigames = false;
    public bool imStillIn = true;
    public bool award = false;
    public Script_MagicHat cardPick = new Script_MagicHat();
    public Script_PlayerMovement move;
    public Script_LevelManager levelManager;
    public AudioSource audioSource;
    public AudioClip annoyingClip;
    public GameObject mySpawnPoint;

    private void Awake()
    {
        if(gameObject.CompareTag("Player1"))
        {
            originalController = 1;
        }
        if (gameObject.CompareTag("Player2"))
        {
            originalController = 2;
        }
        if (gameObject.CompareTag("Player3"))
        {
            originalController = 3;
        }
        if (gameObject.CompareTag("Player4"))
        {
            originalController = 4;
        }

        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        audioSource = GetComponent<AudioSource>();
        move = GetComponent<Script_PlayerMovement>();
    }

    private void Update()
    {
        //if we're not on the gameboard...
        if (SceneManager.GetActiveScene() != 
            SceneManager.GetSceneByName(levelManager.boardName))
        {
            //my target is myself
            GetComponent<Script_PlayerMovement>().target = gameObject.transform;
        }

        //if its not my turn and im in the board game
        if (!myTurn &&
            SceneManager.GetActiveScene() == SceneManager.GetSceneByName(levelManager.boardName))
        {
            //I can press A to be annoying
            if(originalController == 1 && Input.GetKeyUp(KeyCode.Joystick1Button0))
            {
                audioSource.PlayOneShot(annoyingClip, 0.5f);
            }
            if (originalController == 2 && Input.GetKeyUp(KeyCode.Joystick2Button0))
            {
                audioSource.PlayOneShot(annoyingClip, 0.5f);
            }
            if (originalController == 3 && Input.GetKeyUp(KeyCode.Joystick3Button0))
            {
                audioSource.PlayOneShot(annoyingClip, 0.5f);
            }
            if (originalController == 4 && Input.GetKeyUp(KeyCode.Joystick4Button0))
            {
                audioSource.PlayOneShot(annoyingClip, 0.5f);
            }
        }

        if (levelManager.needScene)
        {
            imStillIn = true;
        }

        //if I win the minigame
        if (levelManager.playersStillIn == 1 && imStillIn)
        {
            //and Im in the hot potato minigame
            if (levelManager.minigameName == "Scene_MiniGameHotPotato")
            {
                Gib10Monee();
            }
            //Im in any other minigame
            else if(levelManager.minigameName != "Scene_MiniGameHotPotato")
            {
                Gib30Monee();
            }
        }

        //make sure I never go under zero silver or souls
        if (silverPieces < 0)
            silverPieces = 0;
        if (souls < 0)
            souls = 0;
    }

    /// <summary>
    /// Pick a card from the hat and tell the script "player movement" the
    /// number of the card you picked, then tell yourself to start moving.
    /// </summary>
    public void CardPickAndMovement()
    {
        //mix the cards in the hat
        cardPick.Mix();
        numberOfMoveSpaces = cardPick.PickFromHat();

        //set the number of move spaces pick from the hat
        move = GetComponent<Script_PlayerMovement>();
        move.numberOfSpacesToGo = numberOfMoveSpaces;

        //start moving those number of spaces
        move.StartMoving();
    }

    public void Gib30Monee()
    {
        //I get 30 peices of silver
        silverPieces += 15;
        //Im set back to being in gameboard mode
        imStillIn = false;
        levelManager.gameObject.GetComponent<Script_PlayerManager>().minigameWinner = gameObject;
    }

    public void Gib10Monee()
    {
        //I get 5 peices of silver
        silverPieces += 5;
        //Im set back to being in gameboard mode
        imStillIn = false;
    }
}
