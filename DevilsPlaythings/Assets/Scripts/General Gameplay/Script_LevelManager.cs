﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Script_LevelManager : MonoBehaviour
{
    //public variable
    public static Script_LevelManager instance = null;
    public GameObject[] players = new GameObject[4];
    public List<GameObject> playerOrder;

    public Script_MagicHat deck;
    public Script_Turns currentTurn;
    public Text player1Text;
    public Text player2Text;
    public Text player3Text;
    public Text player4Text;
    public GameObject transitionScreen;
    public bool readyForMiniGames;
    public bool needScene;
    public int turns;
    public int levelImIn = 0;
    public int playersStillIn = 4;
    public int minigamePick = 0;
    public int numberOfTurnsLeft = 10;
    public string boardName;
    public string minigameName;
    public string[] FFAMinigames = new string[4];

    //hidden in inspector
    [HideInInspector]
    public List<GameObject> soulWinners = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> silverWinners = new List<GameObject>();

    public List<GameObject> winners = new List<GameObject>();
    public int numberOfWinners = 0;
    [HideInInspector]
    public bool winnerDecided;

    //private variables
    private bool startGame;
    private bool gameEnded;
    private float delayTimer = 0;

    /// <summary>
    /// Makes the LevelManager a Singleton
    /// </summary>
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Initializes the beginning scene components
    /// </summary>
    void Start()
    {
        numberOfTurnsLeft = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Script_GameManager>().numberOfTurns;
        transitionScreen = GameObject.FindGameObjectWithTag("Transition");
        transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
        playerOrder = new List<GameObject>();
        deck = new Script_MagicHat();
        currentTurn = GetComponent<Script_Turns>();
        needScene = true;
        boardName = SceneManager.GetActiveScene().name;
        currentTurn = GetComponent<Script_Turns>();
        DeterminingTurnOrder();
        FindText();
        FindStar();
        FFAMinigames[0] = "Scene_MiniGameJumpRope";
        FFAMinigames[1] = "Scene_MiniGameSpiritBomb";
        FFAMinigames[2] = "Scene_MiniGameHotPotato";
        FFAMinigames[3] = "Scene_MiniGameDodgeball";
    }

    /// <summary>
    /// Determines the order of the players
    /// </summary>
    void DeterminingTurnOrder()
    {
        //mixes the card of the deck instance
        deck.Mix();

        //takes the top card of the deck
        players[0].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();
        players[1].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();
        players[2].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();
        players[3].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();

        //add in the all the players to a transformable list(playerOrder)
        foreach (GameObject player in players)
        {
            playerOrder.Add(player);
        }

        //orders the player, from highest card to lowest
        playerOrder = playerOrder.OrderByDescending(x => x.GetComponent<Script_Player>().turnOrder).ToList();
        startGame = true;
    }

    private void Update()
    {
        //starts the game when the turn order is figured out
        if (startGame)
        {
            startGame = false;
            currentTurn.StartTurns();
        }

        //if we're in the board game area
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName(boardName))
        {
            if (needScene)
            {
                gameObject.GetComponent<Script_Turns>().idiotScreen = GameObject.Find("Spr_HoldB");
                gameObject.GetComponent<Script_Turns>().idiotScreen.GetComponent<SpriteRenderer>().enabled = false;
                Script_GameManager.PlayBoardMusic();
                FindText();
                currentTurn.playerRoll = GameObject.Find("PlayersRoll").GetComponent<Text>();
                needScene = false;
                //set the players movements
                playerOrder[0].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                playerOrder[1].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                playerOrder[2].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                playerOrder[3].GetComponent<Script_PlayerMovement>().UpdateOnScene();
            }
            if (player1Text != null)
            {
                FindText();
                player1Text.text =
                    "- Pigglicorn - \n" +
                    " Silver: " + players[0].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[0].GetComponent<Script_Player>().souls;
                player2Text.text =
                    "- Shrubbery - \n" +
                    " Silver: " + players[1].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[1].GetComponent<Script_Player>().souls;
                player3Text.text =
                    "- Angel - \n" +
                    " Silver: " + players[2].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[2].GetComponent<Script_Player>().souls;
                player4Text.text =
                    "- Big Bug - \n" +
                    " Silver: " + players[3].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[3].GetComponent<Script_Player>().souls;
            }
            if (numberOfTurnsLeft == 0 && !gameEnded)
            {
                gameEnded = true;
                GameEnded();
                SceneManager.LoadScene("Scene_Win");
                Script_GameManager.PlayMenuMusic();
            }
        }
        //else if we're in a minigame
        else if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName(minigameName))
        {
            transitionScreen = GameObject.FindGameObjectWithTag("Transition");
            transitionScreen.GetComponent<SpriteRenderer>().enabled = false;

            //if players are out
            if (playersStillIn == 1 || playersStillIn == 0)
            {
                transitionScreen.GetComponent<SpriteRenderer>().enabled = true;

                //Starts time after a 3 second delay
                if (delayTimer <= 3.0f)
                {
                    if ((Input.GetKey(KeyCode.Joystick1Button0) &&
                         Input.GetKey(KeyCode.Joystick2Button0) &&
                         Input.GetKey(KeyCode.Joystick3Button0) &&
                         Input.GetKey(KeyCode.Joystick4Button0)) ||
                         Input.GetKey(KeyCode.L))
                    {
                        //finds the minigame script and takes it off the players
                        Script_MinigameCoordinator minigameCoord = GameObject.Find("Camera").GetComponent<Script_MinigameCoordinator>();
                        minigameCoord.Destroy();
                        ///When the minigame ends, turn on the players renderers,
                        ///move them to their spawn points, and destroy any minigame scripts
                        FindPlayerSpawn(0);
                        FindPlayerSpawn(1);
                        FindPlayerSpawn(2);
                        FindPlayerSpawn(3);

                        //load back in the board scene
                        SceneManager.LoadScene("Scene_TransitionWinner");
                        //get the scene neccessities
                        needScene = true;
                        transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
                        //set back to not playing minigames
                        readyForMiniGames = false;
                        //players set for next minigame
                        playersStillIn = 4;
                        //take off a turn left until the games end
                        numberOfTurnsLeft--;                        
                        delayTimer = 0;
                    }
                }
                delayTimer *= Time.deltaTime;
            }
        }


        //if all four players are ready for minigames(they've all taken turns)
        if (playerOrder[0].GetComponent<Script_Player>().readyForMinigames == true &&
           playerOrder[1].GetComponent<Script_Player>().readyForMinigames == true &&
           playerOrder[2].GetComponent<Script_Player>().readyForMinigames == true &&
           playerOrder[3].GetComponent<Script_Player>().readyForMinigames == true)
        {
            //level manager is ready for minigames
            readyForMiniGames = true;
            //all the players are set back to not ready
            playerOrder[0].GetComponent<Script_Player>().readyForMinigames = false;
            playerOrder[1].GetComponent<Script_Player>().readyForMinigames = false;
            playerOrder[2].GetComponent<Script_Player>().readyForMinigames = false;
            playerOrder[3].GetComponent<Script_Player>().readyForMinigames = false;
        }

        //if the level manager is now ready for minigames
        if (readyForMiniGames)
        {
            //turn on transition screen
            transitionScreen.GetComponent<SpriteRenderer>().enabled = true;

            //if all the players hold A and are in the gameboard still
            if (((Input.GetKey(KeyCode.Joystick1Button0) &&
                 Input.GetKey(KeyCode.Joystick2Button0) &&
                 Input.GetKey(KeyCode.Joystick3Button0) &&
                 Input.GetKey(KeyCode.Joystick4Button0)) ||
                 Input.GetKey(KeyCode.L)) &&
                 SceneManager.GetActiveScene() == SceneManager.GetSceneByName(boardName))
            {
                readyForMiniGames = false;
                //turn on transition screen
                transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
                //creates a new random number class
                System.Random randomlevel = new System.Random();
                //finds a random number between your two values(min inclusive,max exclusive)
                minigamePick = randomlevel.Next(0, 4);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                minigameName = FFAMinigames[minigamePick];
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                SceneManager.LoadScene("Scene_Transition");

                ///all the players targets are themselves for duration 
                ///of the minigame, because the target needs to be set 
                ///to something
                playerOrder[0].GetComponent<Script_PlayerMovement>().target = playerOrder[0].transform;
                playerOrder[1].GetComponent<Script_PlayerMovement>().target = playerOrder[1].transform;
                playerOrder[2].GetComponent<Script_PlayerMovement>().target = playerOrder[2].transform;
                playerOrder[3].GetComponent<Script_PlayerMovement>().target = playerOrder[3].transform;
                needScene = false;
            }
        }
    }

    public void GameEnded()
    {
        Debug.Log("GAME HAS ENDED");
        int largestScore = 0;
        bool soulsDone = false;
        bool silversReady = false;
        bool finalSay = false;

        //if you havent caluclated the player with the most souls
        if (soulsDone == false)
        {
            //go through each players souls count to see who has the most
            for (int i = 0; i < playerOrder.Count; i++)
            {
                //if the current iteration player has more souls than the previous score
                if (playerOrder[i].GetComponent<Script_Player>().souls > largestScore)
                {
                    //the largets score is now that players
                    largestScore = playerOrder[i].GetComponent<Script_Player>().souls;
                    //clear out any old winners
                    soulWinners.Clear();
                    soulWinners.Add(playerOrder[i]);
                }
                //else if they players have the same score as previous, add it to the list of winners
                else if (playerOrder[i].GetComponent<Script_Player>().souls == largestScore)
                {
                    soulWinners.Add(playerOrder[i]);
                }
            }
            //you're now done with caluclating souls, on to silver pieces!
            silversReady = true;
            soulsDone = true;
        }

        //now calculating silver pieces(just in case there are two souls winners)
        if (silversReady)
        {
            largestScore = 0;
            //iterated through the people who have the most souls(might only be 1)
            for (int i = 0; i < soulWinners.Count; i++)
            {
                //if the players have more silver pieces than the previous player...
                if (soulWinners[i].GetComponent<Script_Player>().silverPieces > largestScore)
                {
                    //replace them in the winners spot
                    largestScore = soulWinners[i].GetComponent<Script_Player>().silverPieces;
                    //clear out any previous winners
                    silverWinners.Clear();
                    silverWinners.Add(soulWinners[i]);
                }
                //else if they have the same number of silver pieces...
                else if (soulWinners[i].GetComponent<Script_Player>().silverPieces > largestScore)
                {
                    //add them to the winners
                    silverWinners.Add(soulWinners[i]);
                }
            }
            //silver caluclation is done, get ready to find the winner
            finalSay = true;
            silversReady = false;
        }

        if (finalSay)
        {
            //if more than one person has the biggest amount of souls
            if (soulWinners.Count > 1)
            {
                //if one person had the most silver, they win
                if (silverWinners.Count == 1)
                {
                    Debug.Log("The winner is "
                                + silverWinners[0].name);
                    numberOfWinners = 1;
                    winners.Add(silverWinners[0]);
                    winnerDecided = true;
                }
                //if two people tied in silver also, they win
                else if (silverWinners.Count == 2)
                {
                    Debug.Log("The winners are "
                                + silverWinners[0].name +
                        " and " + silverWinners[1].name);
                    numberOfWinners = 2;
                    winners.Add(silverWinners[0]);
                    winners.Add(silverWinners[1]);
                    winnerDecided = true;
                }
                ///if by the slightest change, three people 
                ///had same silver, they win
                else if (silverWinners.Count == 3)
                {
                    Debug.Log("The winners are "
                                + silverWinners[0].name +
                        " and " + silverWinners[1].name +
                        " and " + silverWinners[2].name);
                    numberOfWinners = 3;
                    winners.Add(silverWinners[0]);
                    winners.Add(silverWinners[1]);
                    winners.Add(silverWinners[2]);
                    winnerDecided = true;
                }
                ///if by some fucking miracle, 4 people have both the 
                ///same amount of souls and silver, everyone wins!
                else if (silverWinners.Count == 4)
                {
                    Debug.Log("The winners are "
                                + silverWinners[0].name +
                        " and " + silverWinners[1].name +
                        " and " + silverWinners[2].name +
                        " and " + silverWinners[3].name);
                    numberOfWinners = 4;
                    winners.Add(silverWinners[0]);
                    winners.Add(silverWinners[1]);
                    winners.Add(silverWinners[2]);
                    winners.Add(silverWinners[3]);
                    winnerDecided = true;
                }
            }
            //more than likely, one person will have the most souls, and win
            else if (soulWinners.Count == 1)
            {
                Debug.Log("The winner is " + soulWinners[0].name);
                numberOfWinners = 1;
                winners.Add(soulWinners[0]);
                winnerDecided = true;
            }
        }
    }

    /// <summary>
    /// Finds the text box for each player and enables anything related 
    /// to the players stats
    /// </summary>
    void FindText()
    {
        player1Text = GameObject.Find("Player 1 Coin").GetComponent<Text>();
        player2Text = GameObject.Find("Player 2 Coin").GetComponent<Text>();
        player3Text = GameObject.Find("Player 3 Coin").GetComponent<Text>();
        player4Text = GameObject.Find("Player 4 Coin").GetComponent<Text>();
        transitionScreen = GameObject.FindGameObjectWithTag("Transition");
        transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
    }

    /// <summary>
    /// Player 1 finds a star and lets the other three players know where
    /// its at.
    /// </summary>
    public void FindStar()
    {
        players[0].GetComponent<Script_PlayerMovement>().RandomStar();
        players[1].GetComponent<Script_PlayerMovement>().randomStar =
            players[0].GetComponent<Script_PlayerMovement>().randomStar;
        players[2].GetComponent<Script_PlayerMovement>().randomStar =
            players[0].GetComponent<Script_PlayerMovement>().randomStar;
        players[3].GetComponent<Script_PlayerMovement>().randomStar =
            players[0].GetComponent<Script_PlayerMovement>().randomStar;
    }

    /// <summary>
    /// When players get back to the board, they can know where they left off
    /// </summary>
    /// <param name="playerCount">which player needs their spawn</param>
    public void FindPlayerSpawn(int playerCount)
    {
        playerOrder[playerCount].transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        playerOrder[playerCount].transform.position = playerOrder[playerCount].GetComponent<Script_Player>().mySpawnPoint.transform.position;
        Destroy(playerOrder[playerCount].GetComponent<Script_MinigameCoordinator>());
    }
}
