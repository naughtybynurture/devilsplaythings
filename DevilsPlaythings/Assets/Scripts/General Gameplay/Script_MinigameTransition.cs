﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_MinigameTransition : MonoBehaviour
{
    Script_LevelManager levelManager;
    [Header("Dodgeball Images")]
    public Image dodgeballTitle;
    public Image dodgeballPreview;
    public Image dodgeballInstruction;
    [Header("Jump Rope Images")]
    public Image jumpeTitle;
    public Image jumpPreview;
    public Image jumpInstruction;
    [Header("Heaven Bomb Images")]
    public Image spiritbombTitle;
    public Image spiritbombPreview;
    public Image spiritbombInstruction;
    [Header("Potato Images")]
    public Image hotpotatoTitle;
    public Image hotpotatoPreview;
    public Image hotpotatoInstruction;

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        if (levelManager.minigameName == "Scene_MiniGameDodgeball")
        {
            dodgeballTitle.enabled = true;
            dodgeballPreview.enabled = true;
            dodgeballInstruction.enabled = true;
        }
        else if (levelManager.minigameName == "Scene_MiniGameHotPotato")
        {
            hotpotatoTitle.enabled = true;
            hotpotatoPreview.enabled = true;
            hotpotatoInstruction.enabled = true;
        }
        else if (levelManager.minigameName == "Scene_MiniGameJumpRope")
        {
            jumpeTitle.enabled = true;
            jumpPreview.enabled = true;
            jumpInstruction.enabled = true;
        }
        else if (levelManager.minigameName == "Scene_MiniGameSpiritBomb")
        {
            spiritbombTitle.enabled = true;
            spiritbombPreview.enabled = true;
            spiritbombInstruction.enabled = true;
        }
    }

    private void Update()
    {
        if ((Input.GetKey(KeyCode.Joystick1Button0) &&
            Input.GetKey(KeyCode.Joystick2Button0) &&
            Input.GetKey(KeyCode.Joystick3Button0) &&
            Input.GetKey(KeyCode.Joystick4Button0) )||
            Input.GetKey(KeyCode.L))
        {
            SceneManager.LoadScene(levelManager.minigameName);
            Script_GameManager.PlayMiniGameMusic();
        }
    }
}
