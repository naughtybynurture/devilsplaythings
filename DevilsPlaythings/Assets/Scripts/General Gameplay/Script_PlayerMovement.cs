﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_PlayerMovement : MonoBehaviour
{
    public Script_LevelManager levelManager;
    public GameObject[] path;
    public Transform target;
    public Vector3 startPosition;
    public Vector3 direction;
    public GameObject pauseMenu;
    public int spaceImOn = 0;
    public int numberOfSpacesToGo;
    public float speed;
    public float levelImIn = 0;
    public bool alterMovement;

    public Color myColor;
    public int randomStar = 0;
    public int numberOfBoardSpaces = 0;
    private bool paused = false;

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        if (SceneManager.GetActiveScene().name == "Scene_Test")
        {
            levelImIn = 0;
            numberOfBoardSpaces = 43;
        }
        StartLevel();
        startPosition = path[42].transform.position;      
    }

    public void StartLevel()
    {
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        pauseMenu.GetComponent<Image>().enabled = false;
        pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = false;
        //your new path is an array of gameobject slots
        path = new GameObject[numberOfBoardSpaces];
        //fill the array with the board spaces
        path = GameObject.FindGameObjectsWithTag("Path").OrderBy(go => go.name).ToArray();
        //the target is now the space I want to be on, on the path
        target = path[spaceImOn].transform;
    }

    public void UpdateOnScene()
    {
        paused = false;
        StartLevel();
        path[randomStar].GetComponent<Script_Waypoints>().myCurrentColor = Color.white;
        path[randomStar].GetComponent<Script_Waypoints>().imAStar = true;
        path[randomStar].GetComponent<Script_Waypoints>().myBoardSpace.GetComponent<Renderer>().material.color =
            Color.white;
    }

    /// <summary>
    /// Move the player
    /// </summary>
    public void StartMoving()
    {
        InvokeRepeating("PlayerMove", 0.25f, 0.003f);
    }

    private void PlayerMove()
    {
        //start moving

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName(levelManager.boardName))
        {
            direction = target.GetComponent<Renderer>().bounds.center - transform.position;

            transform.Translate(direction.normalized * speed * Time.deltaTime);

            //if I reach the end of this path
            if (spaceImOn == path.Length - 1)
            {
                //reset to original position
                transform.position = startPosition;
                //the space target for the player is reset
                target = path[0].transform;
                spaceImOn = 0;
            }

            //if I reach the target path space
            if (Vector3.Distance(transform.position, path[spaceImOn].GetComponent<Renderer>().bounds.center) <= 0.07f)
            {
                if (spaceImOn == 30)
                {
                    spaceImOn = 32;
                    target = path[spaceImOn].transform;
                }
                else
                {
                    //increment the index to the next target
                    spaceImOn++;
                }

                if (alterMovement)
                {
                    if (spaceImOn == 12)
                    {
                        spaceImOn = 31;
                        target = path[spaceImOn].transform;
                    }
                }

                //decrements how many space to have left to move
                numberOfSpacesToGo--;
                //move the target
                target = path[spaceImOn].transform;

                GetComponent<Script_Player>().mySpawnPoint.transform.position = gameObject.transform.position;

                //if you hit a star path
                if (path[spaceImOn - 1].GetComponent<Script_Waypoints>().imAStar == true)
                {
                    paused = true;
                }

                //if you have no spaces left to move
                if (numberOfSpacesToGo == 0)
                {
                    //when movement has ended, enable idiot screen for idiots
                    levelManager.gameObject.GetComponent<Script_Turns>().idiotScreen.GetComponent<SpriteRenderer>().enabled = true;
                    levelManager.gameObject.GetComponent<Script_Turns>().turnEnded = true;
                    myColor = path[spaceImOn].GetComponent<Renderer>().material.color;
                    CancelInvoke();
                }
            }
        }
    }

    /// <summary>
    /// When the user presses A during a pause
    /// </summary>
    public void PressA()
    {
        //figure out where the new star will be
        levelManager.FindStar();
        //take five silver pieces way from them
        GetComponent<Script_Player>().silverPieces -= 30;
        //add a soul
        GetComponent<Script_Player>().souls++;
        //disable pause menu
        pauseMenu.GetComponent<Image>().enabled = false;
        pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = false;
        paused = false;
        //turn time back on
        Time.timeScale = 1;
    }

    /// <summary>
    /// When the user presses B during a pause
    /// </summary>
    public void PressB()
    {
        //disable pause menu
        pauseMenu.GetComponent<Image>().enabled = false;
        pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = false;
        paused = false;
        //turn time back on
        Time.timeScale = 1;
    }

    private void Update()
    {
        //if you're not in the board scene(you're in a minigame)
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName(levelManager.boardName))
        {
            //your path will be filled with you's, as a placeholder
            for (int i = 0; i < path.Length; i++)
            {
                path[i] = gameObject;
            }
        }
        //if you are in the board scene
        else
        {
            //if paused
            if (paused)
            {
                //if the game is currently going
                if (Time.timeScale == 1)
                {
                    //enable the pause menu
                    pauseMenu.GetComponent<Image>().enabled = true;
                    pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = true;
                    pauseMenu.transform.GetChild(0).GetComponent<Text>().text = "Would you like to buy a star? It costs 30 silver pieces. Press A to buy, B to exit.";
                    //turn time off
                    Time.timeScale = 0;
                }
                //if the game is already paused
                else if (Time.timeScale == 0)
                {
                    ///Players can press A or B to buy a star to skip if they 
                    ///dont have enough money
                    if (GetComponent<Script_Player>().originalController == 1)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 30)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                    if (GetComponent<Script_Player>().originalController == 2)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick2Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 30)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick2Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                    if (GetComponent<Script_Player>().originalController == 3)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick3Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 30)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick3Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                    if (GetComponent<Script_Player>().originalController == 4)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick4Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 30)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick4Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                }
            }

            //if you're in the Test level
            if (levelImIn == 0)
            {
                
            }
            //else if you're in Limbo
            else if (levelImIn == 1)
            {
                if (alterMovement)
                {
                    if (spaceImOn == 13)
                    {
                        spaceImOn = 34;
                        target = path[spaceImOn].transform;
                    }
                }
            }
        }
    }

    public void SwapColors()
    {
        foreach (GameObject pathTile in path)
        {
            if (pathTile.GetComponent<Renderer>().material.color == Color.blue)
                pathTile.GetComponent<Renderer>().material.color = Color.red;
            else if (pathTile.GetComponent<Renderer>().material.color == Color.red)
                pathTile.GetComponent<Renderer>().material.color = Color.blue;
        }
    }

    public void RandomStar()
    {
        //make sure you change the older star back
        path[randomStar].GetComponent<Renderer>().material.color = path[randomStar].GetComponent<Script_Waypoints>().myOriginalColor;
        path[randomStar].GetComponent<Script_Waypoints>().imAStar = false;
        path[randomStar].GetComponent<Script_Waypoints>().myBoardSpace.GetComponent<Renderer>().material.color =
            path[randomStar].GetComponent<Script_Waypoints>().myOriginalColor;
        

        //create new star space
        System.Random shuffle = new System.Random();
        randomStar = shuffle.Next(0, numberOfBoardSpaces - 1);
        path[randomStar].GetComponent<Script_Waypoints>().myCurrentColor = Color.white;
        path[randomStar].GetComponent<Script_Waypoints>().imAStar = true;
        path[randomStar].GetComponent<Script_Waypoints>().myBoardSpace.GetComponent<Renderer>().material.color = 
            Color.white;
    }
}
