﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_GameManager : MonoBehaviour
{
    public static Script_GameManager instance = null;
    public int numberOfTurns = 0;
    public AudioSource background;
    public AudioClip menuMusic;
    public AudioClip boardMusic;
    public AudioClip minigameMusic;
    public AudioClip youreOut;

    protected virtual void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    protected virtual void Start()
    {

    }

    static public void PlayMenuMusic()
    {
        if (instance != null)
        {
            if (instance.background != null)
            {
                instance.background.Stop();
                instance.background.clip = instance.menuMusic;
                instance.background.Play();
            }
            else
            {
                Debug.LogError("No music component");
            }
        }
    }

    static public void PlayBoardMusic()
    {
        if (instance != null)
        {
            if (instance.background != null)
            {
                instance.background.Stop();
                instance.background.clip = instance.boardMusic;
                instance.background.Play();
            }
            else
            {
                Debug.LogError("No music component");
            }
        }
    }

    static public void PlayMiniGameMusic()
    {
        if (instance != null)
        {
            if (instance.background != null)
            {
                instance.background.Stop();
                instance.background.clip = instance.minigameMusic;
                instance.background.Play();
            }
            else
            {
                Debug.LogError("No music component");
            }
        }
    }
}
