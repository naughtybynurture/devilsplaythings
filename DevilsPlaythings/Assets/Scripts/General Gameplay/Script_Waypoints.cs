﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Waypoints : MonoBehaviour
{
    public Color myCurrentColor;
    public Color myOriginalColor;
    public Script_PlayerManager playerManager;
    public GameObject levelManager;
    public GameObject door1;
    public GameObject door2;
    public GameObject door3;
    public int myGroup;
    public bool imAStar;
    public GameObject myBoardSpace;

    private void Awake()
    {
        myCurrentColor = GetComponent<Renderer>().material.color;
        myOriginalColor = GetComponent<Renderer>().material.color;
        levelManager = GameObject.FindGameObjectWithTag("LevelManager");
        playerManager = levelManager.GetComponent<Script_PlayerManager>();
        if (playerManager.alterPlayersPath == true)
        {
            door1.GetComponent<MeshRenderer>().enabled = true;
            door2.GetComponent<MeshRenderer>().enabled = false;
        }
        if (playerManager.alterPlayersPath == false)
        {
            door1.GetComponent<MeshRenderer>().enabled = false;
            door2.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    private void Update()
    {
        if (playerManager.alterPlayersPath == true)
        {
            door1.GetComponent<MeshRenderer>().enabled = true;
            door2.GetComponent<MeshRenderer>().enabled = false;
        }
        if (playerManager.alterPlayersPath == false)
        {
            door1.GetComponent<MeshRenderer>().enabled = false;
            door2.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        myCurrentColor = GetComponent<Renderer>().material.color;
        //if a space is touched by any of the players
        if (other.gameObject.CompareTag("Player1") ||
           other.gameObject.CompareTag("Player2") ||
           other.gameObject.CompareTag("Player3") ||
           other.gameObject.CompareTag("Player4"))
        {
            //if im on the final space
            if (other.gameObject.GetComponent<Script_PlayerMovement>().numberOfSpacesToGo == 1)
            {
                //if my group is 1
                if (myGroup == 1)
                {
                    //if the players path was normal, we're going to switch the doors
                    if (playerManager.alterPlayersPath == false)
                    {
                        playerManager.alterPlayersPath = true;
                    }
                    else if (playerManager.alterPlayersPath == true)
                    {
                        playerManager.alterPlayersPath = false;
                    }
                }

                //if my color is red
                if (myGroup == 3)
                {
                    //take three silver from the player
                    other.gameObject.GetComponent<Script_Player>().silverPieces -= 3;
                }
                //if my color is blue
                if (myGroup == 2)
                {
                    //give three silver to the player
                    other.gameObject.GetComponent<Script_Player>().silverPieces += 3;
                }

            }
        }
    }
}
