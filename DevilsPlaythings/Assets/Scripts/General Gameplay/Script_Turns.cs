﻿using UnityEngine.UI;
using UnityEngine;

public class Script_Turns : MonoBehaviour
{
    public GameObject player1, player2, player3, player4;
    public GameObject currentPlayer;
    public GameObject idiotScreen;
    public Script_LevelManager players;
    public Text playerRoll;
    public int currentController;
    public bool player1Finished, player2Finished, player3Finished, player4Finished;
    public bool turn1, turn2, turn3, turn4;
    public bool turnEnded;
    public bool onStarSpace;
    public bool hasntRolled = true;

    private void Start()
    {
        //finds the order the players are in
        players = GetComponent<Script_LevelManager>();
        currentPlayer = players.playerOrder[0];
        idiotScreen = GameObject.Find("Spr_HoldB");
        idiotScreen.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void StartTurns()
    {
        //sets the player order
        player1 = players.playerOrder[0];
        player2 = players.playerOrder[1];
        player3 = players.playerOrder[2];
        player4 = players.playerOrder[3];
        //sets turn 1 to true, and others to false
        turn1 = true;
        turn2 = false;
        turn3 = false;
        turn4 = false;
        //sets it so none of the players have finished their turns
        player1Finished = false;
        player2Finished = false;
        player3Finished = false;
        player4Finished = false;
    }

    private void Update()
    {
        //if its turn 1
        if (turn1)
        {
            //if turn 1 has finished
            if (player1Finished)
            {
                //set turn 2 to true
                SwitchTurns();
                turn2 = true;
                PlayerFinished(0);
            }
            //if it's still player 1s turn
            else
            {
                //then the current player is player 1
                currentPlayer = player1;
                PlayerTurnStarted();
            }
        }

        //if its turn 2
        if (turn2)
        {
            //if turn 1 has finished
            if (player2Finished)
            {
                //set turn 3 to true
                SwitchTurns();
                turn3 = true;
                PlayerFinished(1);
            }
            //if it's still player 2s turn
            else
            {
                //then the current player is player 2
                currentPlayer = player2;
                PlayerTurnStarted();
            }
        }

        //if its turn 3
        if (turn3)
        {
            //if turn 3 has finished
            if (player3Finished)
            {
                //set turn 4 to true
                SwitchTurns();
                turn4 = true;
                PlayerFinished(2);
            }
            //if it's still player 3s turn
            else
            {
                //then the current player is player 3
                currentPlayer = player3;
                PlayerTurnStarted();
            }
        }

        //if its turn 4
        if (turn4)
        {
            //if turn 4 has finished
            if (player4Finished)
            {
                //set turns to false
                SwitchTurns();
                PlayerFinished(3);
            }
            //if it's still player 1s turn
            else
            {
                //then the current player is player 4
                currentPlayer = player4;
                PlayerTurnStarted();
            }
        }
    }

    public void SwitchTurns()
    {
        turn1 = false;
        turn2 = false;
        turn3 = false;
        turn4 = false;
    }

    public void PlayerTurnStarted()
    {
        //current controller is player in the first turn orders controller
        currentController = currentPlayer.GetComponent<Script_Player>().originalController;
        //current players tur is on, so they cant make annoying noises
        currentPlayer.GetComponent<Script_Player>().myTurn = true;
        //the gamepag controls
        Controls();
    }

    public void PlayerFinished(int playerOrder)
    {
        //set it so the current player hasn't rolled
        hasntRolled = true;
        players.playerOrder[playerOrder].GetComponent<Script_Player>().readyForMinigames = true;
    }

    public void PressA()
    {
        //set to false so player cant keep pick cards
        hasntRolled = false;
        //Pick a card
        currentPlayer.GetComponent<Script_Player>().CardPickAndMovement();
        //text box shows what the player rolled
        playerRoll.text = 
            currentPlayer.transform.GetChild(0).name + 
            " rolled a " + currentPlayer.GetComponent<Script_Player>().numberOfMoveSpaces;
    }

    public void PressB()
    {
        idiotScreen.GetComponent<SpriteRenderer>().enabled = false;
        //whatever turn it is, the player is finished
        if (turn1)
            player1Finished = true;
        if (turn2)
            player2Finished = true;
        if (turn3)
            player3Finished = true;
        if (turn4)
            player4Finished = true;

        //it is no longer the current players turn
        currentPlayer.GetComponent<Script_Player>().myTurn = false;
        //setting hasnt rolled to true to the next player can roll
        hasntRolled = true;
    }

    public void PressX()
    {
        currentPlayer.GetComponent<Script_Player>().silverPieces -= 30;
        currentPlayer.GetComponent<Script_Player>().souls++;
    }

    public void Controls()
    {
        //if the controller the player is using is number 1
        if (currentController == 1)
        {
            //if its the players turn, the color of the stats changes to red
            if (players.player1Text != null)
            {
                players.player1Text.transform.parent.gameObject.GetComponent<Image>().color = Color.red;
            }

            //if button A is pressed a card is picked from the hat 
            if ((Input.GetKeyUp(KeyCode.Joystick1Button0) || Input.GetKeyUp(KeyCode.Q)) && hasntRolled)
            {
                PressA();
            }
            //if button B is pressed, the turn is over
            if (Input.GetKeyUp(KeyCode.Joystick1Button1) || Input.GetKeyUp(KeyCode.W))
            {
                if (turnEnded)
                {
                    turnEnded = false;
                    //change stat color back to white
                    players.player1Text.transform.parent.gameObject.GetComponent<Image>().color = Color.white;
                    PressB();
                }
            }
            //if button X is pressed, you can pay for a star
            if ((Input.GetKeyUp(KeyCode.Joystick1Button2) || Input.GetKeyUp(KeyCode.E)) && onStarSpace)
            {
                //PressX();
            }
            //if button Y is pressed, progress textboxes/dialogue
            if (Input.GetKeyUp(KeyCode.Joystick1Button3) || Input.GetKeyUp(KeyCode.R))
            {

            }
        }

        //if the controller the player is using is number 2
        if (currentController == 2)
        {
            //if its the players turn, the color of the stats changes to red
            players.player2Text.transform.parent.gameObject.GetComponent<Image>().color = Color.red;

            //if button A is pressed a card is picked from the hat 
            if ((Input.GetKeyUp(KeyCode.Joystick2Button0) || Input.GetKeyUp(KeyCode.Q)) && hasntRolled)
            {
                PressA();
            }
            //if button B is pressed, the turn is over
            if (Input.GetKeyUp(KeyCode.Joystick2Button1) || Input.GetKeyUp(KeyCode.W))
            {
                if (turnEnded)
                {
                    turnEnded = false;
                    //change stat color back to white
                    players.player2Text.transform.parent.gameObject.GetComponent<Image>().color = Color.white;
                    PressB();
                }
            }
            //if button X is pressed, you can pay for a star
            if ((Input.GetKeyUp(KeyCode.Joystick2Button2) || Input.GetKeyUp(KeyCode.E)) && onStarSpace)
            {
                //PressX();
            }
            //if button Y is pressed, progress textboxes/dialogue
            if (Input.GetKeyUp(KeyCode.Joystick2Button3) || Input.GetKeyUp(KeyCode.R))
            {

            }
        }

        //if the controller the player is using is number 3
        if (currentController == 3)
        {
            //if its the players turn, the color of the stats changes to red
            players.player3Text.transform.parent.gameObject.GetComponent<Image>().color = Color.red;

            //if button A is pressed a card is picked from the hat 
            if ((Input.GetKeyUp(KeyCode.Joystick3Button0) || Input.GetKeyUp(KeyCode.Q)) && hasntRolled)
            {
                PressA();
            }
            //if button B is pressed, the turn is over
            if (Input.GetKeyUp(KeyCode.Joystick3Button1) || Input.GetKeyUp(KeyCode.W))
            {
                if (turnEnded)
                {
                    turnEnded = false;
                    //change stat color back to white
                    players.player3Text.transform.parent.gameObject.GetComponent<Image>().color = Color.white;
                    PressB();
                }
            }
            //if button X is pressed, you can pay for a star
            if ((Input.GetKeyUp(KeyCode.Joystick3Button2) || Input.GetKeyUp(KeyCode.E)) && onStarSpace)
            {
                //PressX();
            }
            //if button Y is pressed, progress textboxes/dialogue
            if (Input.GetKeyUp(KeyCode.Joystick3Button3) || Input.GetKeyUp(KeyCode.R))
            {

            }
        }

        //if the controller the player is using is number 4
        if (currentController == 4)
        {
            //if its the players turn, the color of the stats changes to red
            players.player4Text.transform.parent.gameObject.GetComponent<Image>().color = Color.red;

            //if button A is pressed a card is picked from the hat 
            if ((Input.GetKeyUp(KeyCode.Joystick4Button0) || Input.GetKeyUp(KeyCode.Q)) && hasntRolled)
            {
                PressA();
            }
            //if button B is pressed, the turn is over
            if (Input.GetKeyUp(KeyCode.Joystick4Button1) || Input.GetKeyUp(KeyCode.W))
            {
                if (turnEnded)
                {
                    turnEnded = false;
                    //change stat color back to white
                    players.player4Text.transform.parent.gameObject.GetComponent<Image>().color = Color.white;
                    PressB();
                }
            }
            //if button X is pressed, you can pay for a star
            if ((Input.GetKeyUp(KeyCode.Joystick4Button2) || Input.GetKeyUp(KeyCode.E)) && onStarSpace)
            {
                //PressX();
            }
            //if button Y is pressed, progress textboxes/dialogue
            if (Input.GetKeyUp(KeyCode.Joystick4Button3) || Input.GetKeyUp(KeyCode.R))
            {

            }
        }
    }
}
