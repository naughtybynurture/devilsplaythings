﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_MinigameSpiritBomb : MonoBehaviour
{
    
    public Script_LevelManager levelManager;
    public GameObject winningPlayer;
    public GameObject myBomb;
    public GameObject countdownScreen;
    public int winningNumber;
    public int buttonPresses = 0;
    public float countdown = 15.0f;
    public bool winnerChosen = false;

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        winningPlayer = levelManager.playerOrder[0];
        countdownScreen = GameObject.Find("Countdown");
        if (GetComponent<Script_Player>().originalController == 1)
        {
            myBomb = GameObject.Find("Bomb1");
        }
        if (GetComponent<Script_Player>().originalController == 2)
        {
            myBomb = GameObject.Find("Bomb2");
        }
        if (GetComponent<Script_Player>().originalController == 3)
        {
            myBomb = GameObject.Find("Bomb3");
        }
        if (GetComponent<Script_Player>().originalController == 4)
        {
            myBomb = GameObject.Find("Bomb4");
        }
    }

    private void Update()
    {
        if (countdown > 0)
        {
            //user inputs
            if (Input.GetKeyDown(KeyCode.Joystick1Button0) &&
                GetComponent<Script_Player>().originalController == 1)
            {
                myBomb.transform.localScale += new Vector3(0.02f, 0.02f, 0.02f);
                buttonPresses++;
            }
            if (Input.GetKeyDown(KeyCode.Joystick2Button0) &&
                GetComponent<Script_Player>().originalController == 2)
            {
                myBomb.transform.localScale += new Vector3(0.02f, 0.02f, 0.02f);
                buttonPresses++;
            }
            if (Input.GetKeyDown(KeyCode.Joystick3Button0) &&
                GetComponent<Script_Player>().originalController == 3)
            {
                myBomb.transform.localScale += new Vector3(0.02f, 0.02f, 0.02f);
                buttonPresses++;
            }
            if (Input.GetKeyDown(KeyCode.Joystick4Button0) &&
                GetComponent<Script_Player>().originalController == 4)
            {
                myBomb.transform.localScale += new Vector3(0.02f, 0.02f, 0.02f);
                buttonPresses++;
            }
            countdown -= 1.0f * Time.deltaTime;
            countdownScreen.GetComponent<Text>().text = Mathf.Round(countdown) + " seconds left!";
        }
        else
        {
            if (!winnerChosen)
            {
                winnerChosen = true;
                for (int i = 0; i < levelManager.playerOrder.Count; i++)
                {
                    if (levelManager.playerOrder[i].GetComponent<Script_MinigameSpiritBomb>().buttonPresses > winningNumber)
                    {
                        winningNumber = levelManager.playerOrder[i].GetComponent<Script_MinigameSpiritBomb>().buttonPresses;
                        winningPlayer = levelManager.playerOrder[i];
                    }
                }
                GetComponent<Script_Player>().imStillIn = false;
                winningPlayer.GetComponent<Script_Player>().imStillIn = true;
                levelManager.playersStillIn = 1;
            }
        }
    }
}
