﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_PlayerDodge : MonoBehaviour
{
    public Script_Player player;
    public Script_LevelManager levelManager;
    public bool canGoLeft = true;
    public bool canGoRight = true;
    public bool hit;

	// Use this for initialization
	void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        player = GetComponent<Script_Player>();
        //Destroy(GetComponent<Rigidbody>());
	}
	
	// Update is called once per frame
	void Update ()
    {      
        //if I'm player 1
        if (player.originalController == 1)
        {
            if (Input.GetKey(KeyCode.Joystick1Button4))
            {
                GoLeft();
            }
            else if (Input.GetKey(KeyCode.Joystick1Button5))
            {
                GoRight();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick1Button4))
            {
                GoLeft();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick1Button5))
            {
                GoRight();
            }
        }

        //if I'm player 2
        if (player.originalController == 2)
        {
            if (Input.GetKey(KeyCode.Joystick2Button4))
            {
                GoLeft();
            }
            else if (Input.GetKey(KeyCode.Joystick2Button5))
            {
                GoRight();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick2Button4))
            {
                GoLeft();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick2Button5))
            {
                GoRight();
            }
        }

        //if I'm player 3
        if (player.originalController == 3)
        {
            if (Input.GetKey(KeyCode.Joystick3Button4))
            {
                GoLeft();
            }
            else if (Input.GetKey(KeyCode.Joystick3Button5))
            {
                GoRight();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick3Button4))
            {
                GoLeft();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick3Button5))
            {
                GoRight();
            }
        }

        //if I'm player 4
        if (player.originalController == 4)
        {
            if (Input.GetKey(KeyCode.Joystick4Button4))
            {
                GoLeft();
            }
            else if (Input.GetKey(KeyCode.Joystick4Button5))
            {
                GoRight();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick4Button4))
            {
                GoLeft();
            }
            else if (Input.GetKeyDown(KeyCode.Joystick4Button5))
            {
                GoRight();
            }
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "WallLeft")
        {
            canGoLeft = false;
        }
        if (collision.gameObject.name == "WallRight")
        {
            canGoRight = false;
        }   
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.name == "WallLeft")
        {
            canGoLeft = true;
        }
        if (collision.gameObject.name == "WallRight")
        {
            canGoRight = true;
        }
    }

    public void GoLeft()
    {
        if (canGoLeft)
        {
            transform.Translate(new Vector3(-0.05f, 0));
        }
    }

    public void GoRight()
    {
        if (canGoRight)
        {
            transform.Translate(new Vector3(0.05f, 0));
        }
    }
}
