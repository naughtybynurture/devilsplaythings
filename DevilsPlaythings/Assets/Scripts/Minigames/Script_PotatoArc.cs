﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_PotatoArc : MonoBehaviour
{
    public Vector3 startPosition = new Vector3(0, 0, 0);
    public Vector3 endPosition = new Vector3(0, 0, 10);
    public bool throwingPotato = false;
    float trajectoryHeight = 0.5f;
    float timer = 0;
    float potatoSpeed = 0.05f;
    bool needToMakeNoise = true;
    public AudioSource myself;
    public AudioClip bombToss;

    private void Start()
    {
        startPosition = GameObject.FindGameObjectWithTag("Player1").transform.position;   
    }

    private void Update()
    {
        //if a potato is being thrown
        if (throwingPotato)
        {
            if(needToMakeNoise)
            {
                needToMakeNoise = false;
                myself.PlayOneShot(bombToss, 0.5f);
            }
            //add to the amount of time the potato travels
            timer += potatoSpeed;
            //calculate straight-line lerp position;
            Vector3 currentPosition = Vector3.Lerp(startPosition, endPosition, timer);
            //add a value to Y, using Sine to give a curved trajectory in the Y direction
            currentPosition.y += trajectoryHeight * Mathf.Sin(Mathf.Clamp01(timer) * Mathf.PI);
            //Assign the new position to the game object
            transform.position = currentPosition;
        }
        //if the potato finally reaches the target...
        if (transform.position == endPosition)
        {
            //no longer move potato
            throwingPotato = false;
            //reset timer
            timer = 0;
            //the start position is our current position
            startPosition = transform.position;
            needToMakeNoise = true;
        }
    }
}
