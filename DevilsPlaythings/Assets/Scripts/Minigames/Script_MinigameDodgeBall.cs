﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_MinigameDodgeBall : MonoBehaviour
{
    public Script_LevelManager levelManager;
    public Script_GameManager gameManager;
    public float speed;
    private float timer = 15.0f;
    public float bumperForce = 0.007f;
    public Vector3 startPosition;
    public GameObject dodgeballPrefab;
    public float absoluteDelay = 90;
    public AudioSource myself;
    public AudioClip ballBounce;

    float timerDelay = 3f;
    bool ballThrown = true;

    // Use this for initialization
    void Start()
    {
        
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        gameManager = GameObject.Find("GameManager").GetComponent<Script_GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //absolute amount of time the balls can instantiate another ball
        if (absoluteDelay > 0f)
        {
            //small delay for first ball to be thrown
            if (timerDelay <= 0 && ballThrown)
            {
                ballThrown = false;
                Throw();
            }
            if (timerDelay <= 0.0f)
            {
                //every 15 seconds a ball instantiates two other balls
                if (timer <= 0)
                {
                    //add another ball in
                    AddDodgeBall();
                    //set time back to 15 seconds
                    timer = 15.0f;
                }
                //else, keep the time counting down
                else
                {
                    timer -= Time.deltaTime;
                }

                //if the bumper force ever gets too high
                if (bumperForce > 0.007f)
                {
                    //set it back down 
                    bumperForce = 0.006f;
                }
                //else if bumper force ever gets too low
                else if (bumperForce <= 0.003)
                {
                    //set it back up
                    bumperForce = 0.004f;
                }

                //if speed ever gets too high
                if (speed > 0.07f)
                {
                    speed = 0.06f;
                }
                //if speed ever gets too low
                else if (speed < 0.03f)
                {
                    speed = 0.04f;
                }
            }
            timerDelay -= 1.0f * Time.deltaTime;
            absoluteDelay -= 1.0f * Time.deltaTime;
        }
    }

    /// <summary>
    /// Throw the ball after a small delay
    /// </summary>
    public void Throw()
    {
        startPosition = gameObject.transform.position;
        speed = 0.05f;
        GetComponent<Rigidbody>().AddForce(new Vector3(0.1f, 0, 0.1f));
    }


    /// <summary>
    /// If the ball ever comes in contact with a wall, add force
    /// </summary>
    /// <param name="collision">an object the ball collides with</param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            myself.PlayOneShot(ballBounce, 0.25f);
            GetComponent<Rigidbody>().AddForce(collision.contacts[0].normal * bumperForce, ForceMode.Impulse);
        }
    }

    /// <summary>
    /// If the ball ever touches a player
    /// </summary>
    /// <param name="other">an object the ball passes through</param>
    private void OnTriggerEnter(Collider other)
    {
        //if the ball touches a player
        if (other.gameObject.CompareTag("Player1") ||
            other.gameObject.CompareTag("Player2") ||
            other.gameObject.CompareTag("Player3") ||
            other.gameObject.CompareTag("Player4"))
        {
            //disable their mesh renderer
            other.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            //count that player as not in
            other.GetComponent<Script_Player>().imStillIn = false;
            //if there are two or more players...
            if (other.GetComponent<Script_PlayerDodge>().hit == false && levelManager.playersStillIn >= 2)
            {
                myself.PlayOneShot(gameManager.youreOut, 0.5f);
                //set the players hit boolean to true.
                other.GetComponent<Script_PlayerDodge>().hit = true;
                //make sure the level manager knows someone is out
                levelManager.playersStillIn--;
            }
        }
    }

    /// <summary>
    /// instaniates a dodgeball
    /// </summary>
    public void AddDodgeBall()
    {
        Instantiate(dodgeballPrefab, startPosition, Quaternion.identity);
    }
}
