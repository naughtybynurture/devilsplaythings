﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_MinigameHotPotato: MonoBehaviour
{
    public GameObject potato;
    public Script_LevelManager levelManager;
    public Script_Player myPlayer;
    public GameObject countdownScreen;
    public bool iHaveThePotato;
    public float countdown = 15.0f;

	// Use this for initialization
	void Start ()
    {
        countdownScreen = GameObject.Find("Countdown");
        potato = GameObject.Find("Potato");
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        levelManager.playerOrder[0].GetComponent<Script_MinigameHotPotato>().iHaveThePotato = true;
        myPlayer = GetComponent<Script_Player>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (countdown > 0)
        {
            if (iHaveThePotato)
            {
                //if Im Lil Pig
                if (myPlayer.originalController == 1)
                {
                    if (Input.GetKeyDown(KeyCode.Joystick1Button1))
                    {
                        StartPosition(1);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick1Button2))
                    {
                        StartPosition(2);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick1Button3))
                    {
                        StartPosition(3);
                    }
                }

                //if Im Demon
                if (myPlayer.originalController == 2)
                {
                    if (Input.GetKeyDown(KeyCode.Joystick2Button0))
                    {
                        StartPosition(0);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick2Button2))
                    {
                        StartPosition(2);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick2Button3))
                    {
                        StartPosition(3);
                    }
                }

                //if Im Tree
                if (myPlayer.originalController == 3)
                {
                    if (Input.GetKeyDown(KeyCode.Joystick3Button0))
                    {
                        StartPosition(0);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick3Button1))
                    {
                        StartPosition(1);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick3Button3))
                    {
                        StartPosition(3);
                    }
                }

                //if Im Wario
                if (myPlayer.originalController == 4)
                {
                    if (Input.GetKeyDown(KeyCode.Joystick4Button0))
                    {
                        StartPosition(0);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick4Button1))
                    {
                        StartPosition(1);
                    }
                    if (Input.GetKeyDown(KeyCode.Joystick4Button2))
                    {
                        StartPosition(2);
                    }
                }
            }
            countdown -= 1.0f * Time.deltaTime;
            countdownScreen.GetComponent<Text>().text = Mathf.Round(countdown) + " seconds left!";
        }
        else
        {
            if(iHaveThePotato)
            {
                myPlayer.imStillIn = false;
                levelManager.gameObject.GetComponent<Script_PlayerManager>().minigameWinner = gameObject;
                levelManager.playersStillIn = 1;
            }
        }
	}

    public void StartPosition(int playerorder)
    {
        iHaveThePotato = false;
        potato.GetComponent<Script_PotatoArc>().throwingPotato = true;
        potato.GetComponent<Script_PotatoArc>().endPosition = levelManager.playerOrder[playerorder].transform.position;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("potato"))
        {
            if (potato.GetComponent<Script_PotatoArc>().throwingPotato == false)
            {
                iHaveThePotato = true;
            }
        }
    }
}
